FROM fedora

LABEL maintainer="Furutanian <furutanian@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		ruby \
		ruby-devel \
		gcc \
		redhat-rpm-config \
		zlib-devel \
		patch \
		make \
		gcc-c++ \
		mysql++-devel \
		procps-ng \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all
RUN set -x \
	&& gem install \
		bundler

# git clone hiki-rack しておくこと
COPY hiki-rack /home/user/hiki
RUN set -x \
	&& /usr/sbin/useradd user \
	&& /usr/bin/chown -R user:user /home/user
USER user
WORKDIR /home/user/hiki
RUN set -x \
	&& bundle config set path ./bundle \
	&& bundle install \
	&& bundle clean -V

EXPOSE 8080

ENTRYPOINT ["bundle", "exec", "rackup -E production -P /tmp/rack.pid --host 0.0.0.0 --port 8080"]

