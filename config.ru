#!/usr/bin/env rackup
# -*- ruby -*-

$VERBOSE = nil
Encoding.default_external = 'UTF-8'

pwd = `pwd`.chomp.split('/')
cdir = pwd[0..-1].join('/')
pdir = pwd[0..-2].join('/')
pdir.size == 0 and pdir = '/tmp'

unless(FileTest.exist?('%s/data/text' % pdir))
	puts('make data directory at [%s]' % pdir)
	system('/bin/mkdir -p %s/data' % pdir)
	system('/bin/cp -a %s/data/* %s/data'% [cdir, pdir])
end

unless(FileTest.exist?('%s/hikiconf.rb' % cdir))
	puts('make config file at [%s]' % cdir)
	system('/bin/cat %s/hikiconf.rb.sample | /bin/sed "s/^@data_path.*/@data_path       = \'%s\/data\'/" > %s/hikiconf.rb' % [cdir, pdir.gsub(/\//, '\/'), cdir])
end

$LOAD_PATH.unshift "lib"

require "hiki/app"
require "hiki/attachment"

use Rack::Lint
use Rack::ShowExceptions
use Rack::Reloader
# use Rack::Session::Cookie
# use Rack::ShowStatus
use Rack::CommonLogger
use Rack::Static, :urls => ["/theme"], :root => "."

map "/" do
  run Hiki::App.new("hikiconf.rb")
end
map "/attach" do
  run Hiki::Attachment.new("hikiconf.rb")
end
